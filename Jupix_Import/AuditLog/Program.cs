﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace AuditLog
{

    //public class FullDescription
    //{
    //    [JsonProperty("#cdata-section")]
    //    public object CdataSection { get; set; }
    //}

    //public class Image
    //{
    //    [JsonProperty("@modified")]
    //    public string Modified { get; set; }

    //    [JsonProperty("#text")]
    //    public string Text { get; set; }
    //}

    //public class Images
    //{
    //    public List<Image> image { get; set; }
    //}

    //public class Epc
    //{
    //    public object buildingName { get; set; }
    //    public string eerCurrentLetter { get; set; }
    //    public string eerCurrentValue { get; set; }
    //    public string eerPotentialLetter { get; set; }
    //    public string eerPotentialValue { get; set; }
    //    public object eirCurrentLetter { get; set; }
    //    public object eirCurrentValue { get; set; }
    //    public object eirPotentialLetter { get; set; }
    //    public object eirPotentialValue { get; set; }
    //}

    //public class Brochure
    //{
    //    [JsonProperty("@modified")]
    //    public string Modified { get; set; }

    //    [JsonProperty("#text")]
    //    public string Text { get; set; }
    //}

    //public class Brochures
    //{
    //    //public Brochure brochure { get; set; }
    //}

    //public class Property
    //{
    //    public string propertyID { get; set; }
    //    public string branchID { get; set; }
    //    public string clientName { get; set; }
    //    public string branchName { get; set; }
    //    public string department { get; set; }
    //    public string referenceNumber { get; set; }
    //    public string addressName { get; set; }
    //    public string addressNumber { get; set; }
    //    public string addressStreet { get; set; }
    //    public string address2 { get; set; }
    //    public string address3 { get; set; }
    //    public string address4 { get; set; }
    //    public string addressPostcode { get; set; }
    //    public string country { get; set; }
    //    public string displayAddress { get; set; }
    //    public string propertyBedrooms { get; set; }
    //    public string propertyBathrooms { get; set; }
    //    public string propertyEnsuites { get; set; }
    //    public string propertyReceptionRooms { get; set; }
    //    public string propertyKitchens { get; set; }
    //    public string displayPropertyType { get; set; }
    //    public string propertyType { get; set; }
    //    public string propertyStyle { get; set; }
    //    public string propertyAge { get; set; }
    //    public string floorArea { get; set; }
    //    public string floorAreaUnits { get; set; }
    //    public string propertyFeature1 { get; set; }
    //    public string propertyFeature2 { get; set; }
    //    public string propertyFeature3 { get; set; }
    //    public string propertyFeature4 { get; set; }
    //    public string propertyFeature5 { get; set; }
    //    public string propertyFeature6 { get; set; }
    //    public string propertyFeature7 { get; set; }
    //    public string propertyFeature8 { get; set; }
    //    public string propertyFeature9 { get; set; }
    //    public string propertyFeature10 { get; set; }
    //    public string price { get; set; }
    //    public string forSalePOA { get; set; }
    //    public string priceQualifier { get; set; }
    //    public string propertyTenure { get; set; }
    //    public string saleBy { get; set; }
    //    public string developmentOpportunity { get; set; }
    //    public string investmentOpportunity { get; set; }
    //    public string estimatedRentalIncome { get; set; }
    //    public string availability { get; set; }
    //    public string mainSummary { get; set; }
    //    public FullDescription fullDescription { get; set; }
    //    public string dateLastModified { get; set; }
    //    public string timeLastModified { get; set; }
    //    public string featuredProperty { get; set; }
    //    public string regionID { get; set; }
    //    public string latitude { get; set; }
    //    public string longitude { get; set; }
    //    public object flags { get; set; }
    //    public Images images { get; set; }
    //    public object floorplans { get; set; }
    //    public Epc epc { get; set; }
    //    public object epcGraphs { get; set; }
    //    public string epcFrontPages { get; set; }
    //    public Brochures brochures { get; set; }
    //    public object virtualTours { get; set; }
    //    public object externalLinks { get; set; }
    //}

    //public class Properties
    //{
    //    public List<Property> property { get; set; }
    //}

    //public class Root
    //{
    //    public Properties properties { get; set; }
    //}

    //xml
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Root));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Root)serializer.Deserialize(reader);
    // }



    [XmlRoot(ElementName = "image")]
    public class Image
    {

        [XmlAttribute(AttributeName = "modified")]
        public string Modified { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "images")]
    public class Images
    {

        [XmlElement(ElementName = "image")]
        public List<Image> Image { get; set; }
    }

    [XmlRoot(ElementName = "floorplan")]
    public class Floorplan
    {

        [XmlAttribute(AttributeName = "modified")]
        public string Modified { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "floorplans")]
    public class Floorplans
    {

        [XmlElement(ElementName = "floorplan")]
        public Floorplan Floorplan { get; set; }
    }

    [XmlRoot(ElementName = "epc")]
    public class Epc
    {

        [XmlElement(ElementName = "buildingName")]
        public object BuildingName { get; set; }

        [XmlElement(ElementName = "eerCurrentLetter")]
        public string EerCurrentLetter { get; set; }

        [XmlElement(ElementName = "eerCurrentValue")]
        public string EerCurrentValue { get; set; }

        [XmlElement(ElementName = "eerPotentialLetter")]
        public string EerPotentialLetter { get; set; }

        [XmlElement(ElementName = "eerPotentialValue")]
        public string EerPotentialValue { get; set; }

        [XmlElement(ElementName = "eirCurrentLetter")]
        public object EirCurrentLetter { get; set; }

        [XmlElement(ElementName = "eirCurrentValue")]
        public object EirCurrentValue { get; set; }

        [XmlElement(ElementName = "eirPotentialLetter")]
        public object EirPotentialLetter { get; set; }

        [XmlElement(ElementName = "eirPotentialValue")]
        public object EirPotentialValue { get; set; }
    }

    [XmlRoot(ElementName = "epcGraph")]
    public class EpcGraph
    {

        [XmlAttribute(AttributeName = "modified")]
        public string Modified { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "epcGraphs")]
    public class EpcGraphs
    {

        [XmlElement(ElementName = "epcGraph")]
        public EpcGraph EpcGraph { get; set; }
    }

    [XmlRoot(ElementName = "brochure")]
    public class Brochure
    {

        [XmlAttribute(AttributeName = "modified")]
        public string Modified { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "brochures")]
    public class Brochures
    {

        [XmlElement(ElementName = "brochure")]
        public List<Brochure> Brochure { get; set; }
    }

    [XmlRoot(ElementName = "property")]
    public class Property
    {

        [XmlElement(ElementName = "virtualTours")]
        public VirtualTours VirtualTours { get; set; }

        [XmlElement(ElementName = "externalLinks")]
        public object ExternalLinks { get; set; }

        [XmlElement(ElementName = "propertyID")]
        public double PropertyID { get; set; }

        [XmlElement(ElementName = "branchID")]
        public int BranchID { get; set; }

        [XmlElement(ElementName = "clientName")]
        public string ClientName { get; set; }

        [XmlElement(ElementName = "branchName")]
        public string BranchName { get; set; }

        [XmlElement(ElementName = "department")]
        public string Department { get; set; }

        [XmlElement(ElementName = "referenceNumber")]
        public int ReferenceNumber { get; set; }

        [XmlElement(ElementName = "addressName")]
        public string AddressName { get; set; }

        [XmlElement(ElementName = "addressNumber")]
        public string AddressNumber { get; set; }

        [XmlElement(ElementName = "addressStreet")]
        public string AddressStreet { get; set; }

        [XmlElement(ElementName = "address2")]
        public string Address2 { get; set; }

        [XmlElement(ElementName = "address3")]
        public string Address3 { get; set; }

        [XmlElement(ElementName = "address4")]
        public string Address4 { get; set; }

        [XmlElement(ElementName = "addressPostcode")]
        public string AddressPostcode { get; set; }

        [XmlElement(ElementName = "country")]
        public string Country { get; set; }

        [XmlElement(ElementName = "displayAddress")]
        public string DisplayAddress { get; set; }

        [XmlElement(ElementName = "propertyBedrooms")]
        public int PropertyBedrooms { get; set; }

        [XmlElement(ElementName = "propertyBathrooms")]
        public int PropertyBathrooms { get; set; }

        [XmlElement(ElementName = "propertyEnsuites")]
        public int PropertyEnsuites { get; set; }

        [XmlElement(ElementName = "propertyReceptionRooms")]
        public int PropertyReceptionRooms { get; set; }

        [XmlElement(ElementName = "propertyKitchens")]
        public int PropertyKitchens { get; set; }

        [XmlElement(ElementName = "displayPropertyType")]
        public string DisplayPropertyType { get; set; }

        [XmlElement(ElementName = "propertyType")]
        public int? PropertyType { get; set; }

        [XmlElement(ElementName = "propertyStyle")]
        public int PropertyStyle { get; set; }

        [XmlElement(ElementName = "propertyAge")]
        public int PropertyAge { get; set; }

        [XmlElement(ElementName = "floorArea")]
        public double FloorArea { get; set; }

        [XmlElement(ElementName = "floorAreaUnits")]
        public string FloorAreaUnits { get; set; }

        [XmlElement(ElementName = "propertyFeature1")]
        public string PropertyFeature1 { get; set; }

        [XmlElement(ElementName = "propertyFeature2")]
        public string PropertyFeature2 { get; set; }

        [XmlElement(ElementName = "propertyFeature3")]
        public string PropertyFeature3 { get; set; }

        [XmlElement(ElementName = "propertyFeature4")]
        public string PropertyFeature4 { get; set; }

        [XmlElement(ElementName = "propertyFeature5")]
        public string PropertyFeature5 { get; set; }

        [XmlElement(ElementName = "propertyFeature6")]
        public string PropertyFeature6 { get; set; }

        [XmlElement(ElementName = "propertyFeature7")]
        public string PropertyFeature7 { get; set; }

        [XmlElement(ElementName = "propertyFeature8")]
        public string PropertyFeature8 { get; set; }

        [XmlElement(ElementName = "propertyFeature9")]
        public string PropertyFeature9 { get; set; }

        [XmlElement(ElementName = "propertyFeature10")]
        public string PropertyFeature10 { get; set; }

        [XmlElement(ElementName = "price")]
        public int Price { get; set; }

        [XmlElement(ElementName = "forSalePOA")]
        public int ForSalePOA { get; set; }

        [XmlElement(ElementName = "priceQualifier")]
        public int PriceQualifier { get; set; }

        [XmlElement(ElementName = "propertyTenure")]
        public int? PropertyTenure { get; set; }

        [XmlElement(ElementName = "saleBy")]
        public int SaleBy { get; set; }

        [XmlElement(ElementName = "developmentOpportunity")]
        public int DevelopmentOpportunity { get; set; }

        [XmlElement(ElementName = "investmentOpportunity")]
        public int InvestmentOpportunity { get; set; }

        [XmlElement(ElementName = "estimatedRentalIncome")]
        public double EstimatedRentalIncome { get; set; }

        [XmlElement(ElementName = "availability")]
        public int Availability { get; set; }

        [XmlElement(ElementName = "mainSummary")]
        public string MainSummary { get; set; }

        [XmlElement(ElementName = "fullDescription")]
        public string FullDescription { get; set; }

        [XmlElement(ElementName = "dateLastModified")]
        public string DateLastModified { get; set; }

        [XmlElement(ElementName = "timeLastModified")]
        public string TimeLastModified { get; set; }

        [XmlElement(ElementName = "featuredProperty")]
        public int FeaturedProperty { get; set; }

        [XmlElement(ElementName = "regionID")]
        public int RegionID { get; set; }

        [XmlElement(ElementName = "latitude")]
        public string Latitude { get; set; }

        [XmlElement(ElementName = "longitude")]
        public string Longitude { get; set; }

        [XmlElement(ElementName = "flags")]
        public Flags Flags { get; set; }

        [XmlElement(ElementName = "images")]
        public Images Images { get; set; }

        [XmlElement(ElementName = "floorplans")]
        public Floorplans Floorplans { get; set; }

        [XmlElement(ElementName = "epc")]
        public Epc Epc { get; set; }

        [XmlElement(ElementName = "epcGraphs")]
        public EpcGraphs EpcGraphs { get; set; }

        [XmlElement(ElementName = "epcFrontPages")]
        public object EpcFrontPages { get; set; }

        [XmlElement(ElementName = "brochures")]
        public Brochures Brochures { get; set; }

        [XmlElement(ElementName = "rent")]
        public int Rent { get; set; }

        [XmlElement(ElementName = "rentFrequency")]
        public int RentFrequency { get; set; }

        [XmlElement(ElementName = "toLetPOA")]
        public int ToLetPOA { get; set; }

        [XmlElement(ElementName = "studentProperty")]
        public string StudentProperty { get; set; }

        [XmlElement(ElementName = "forSale")]
        public int ForSale { get; set; }

        [XmlElement(ElementName = "toLet")]
        public int ToLet { get; set; }

        [XmlElement(ElementName = "priceTo")]
        public int PriceTo { get; set; }

        [XmlElement(ElementName = "priceFrom")]
        public int PriceFrom { get; set; }

        [XmlElement(ElementName = "rentTo")]
        public int RentTo { get; set; }

        [XmlElement(ElementName = "rentFrom")]
        public int RentFrom { get; set; }

        [XmlElement(ElementName = "floorAreaTo")]
        public double FloorAreaTo { get; set; }

        [XmlElement(ElementName = "floorAreaFrom")]
        public double FloorAreaFrom { get; set; }

        [XmlElement(ElementName = "siteArea")]
        public double SiteArea { get; set; }

        [XmlElement(ElementName = "siteAreaUnits")]
        public string SiteAreaUnits { get; set; }

        [XmlElement(ElementName = "strapLine")]
        public double StrapLine { get; set; }

        [XmlElement(ElementName = "propertyTypes")]
        public PropertyTypes PropertyTypes { get; set; }

        [XmlElement(ElementName = "lettingFeePolicyHeadline")]
        public string lettingFeePolicyHeadline { get; set; }

        [XmlElement(ElementName = "lettingFeePolicyDetails")]
        public string lettingFeePolicyDetails { get; set; }

        [XmlElement(ElementName = "landAreaTo")]
        public decimal landAreaTo { get; set; }

        [XmlElement(ElementName = "landAreaFrom")]
        public decimal landAreaFrom { get; set; }

        [XmlElement(ElementName = "landAreaUnits")]
        public decimal landAreaUnits { get; set; }
    }


    [XmlRoot(ElementName = "flags")]
    public class Flags
    {

        [XmlElement(ElementName = "flag")]
        public string Flag { get; set; }
    }

    [XmlRoot(ElementName = "virtualTour")]
    public class VirtualTour
    {

        [XmlAttribute(AttributeName = "modified")]
        public string Modified { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "virtualTours")]
    public class VirtualTours
    {

        [XmlElement(ElementName = "virtualTour")]
        public List<VirtualTour> VirtualTour { get; set; }
    }

    [XmlRoot(ElementName = "externalLink")]
    public class ExternalLink
    {

        [XmlElement(ElementName = "url")]
        public string Url { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
    }

    [XmlRoot(ElementName = "externalLinks")]
    public class ExternalLinks
    {

        [XmlElement(ElementName = "externalLink")]
        public ExternalLink ExternalLink { get; set; }
    }

    [XmlRoot(ElementName = "propertyTypes")]
    public class PropertyTypes
    {

        [XmlElement(ElementName = "propertyType")]
        public int PropertyType { get; set; }
    }

    [XmlRoot(ElementName = "properties")]
    public class Properties
    {

        [XmlElement(ElementName = "property")]
        public List<Property> Property { get; set; }
    }



    class Program
    {
        static void Main(string[] args)
        {
            ReadXML();
            Console.ReadLine();
        }
        private static void ReadXML()
        {
            // To convert an XML node contained in string xml into a JSON string   

            //using (XmlReader reader = XmlReader.Create(@"F:\Projects\Rajesh And Banga\jupix_feed_sample.xml"))
            //{
            //    XmlDocument doc = new XmlDocument();
            //    String xmlFile = System.IO.File.ReadAllText(@"F:\Projects\Rajesh And Banga\jupix_feed_sample.xml").Replace("&", "");
            //    doc.LoadXml(xmlFile);
            //    string jsonText = JsonConvert.SerializeXmlNode(doc);
            //    var settings = new JsonSerializerSettings
            //    {
            //        NullValueHandling = NullValueHandling.Ignore,
            //        MissingMemberHandling = MissingMemberHandling.Ignore
            //    };
            //    //Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(jsonText, settings); 

            //}
            try
            {
                bool exists = System.IO.Directory.Exists(ConfigurationManager.AppSettings["FileLocation"]);
                if (!exists)
                    System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["FileLocation"]);

                WebClient webClient = new WebClient();
                string xmlUrl = Convert.ToString(ConfigurationManager.AppSettings["xmlUrl"]) + "&" + Convert.ToString(ConfigurationManager.AppSettings["passphrase"]);
                webClient.DownloadFileAsync(new Uri(xmlUrl), ConfigurationManager.AppSettings["FileLocation"] + "\\jupix_feed_sample.xml");
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            }
            catch (Exception)
            {

                throw;
            }
           

        }

        private static void xmlRead()
        {

            XmlDocument doc = new XmlDocument();
            String xmlFile = System.IO.File.ReadAllText(ConfigurationManager.AppSettings["FileLocation"] + "\\jupix_feed_sample.xml");
            if (xmlFile != "")
            {
                doc.LoadXml(xmlFile);
                XmlNodeList properties = doc.SelectNodes("properties");
                XmlNodeList property = properties[0].SelectNodes("property");
                Properties objProperty = new Properties();
                objProperty.Property = new List<Property>();
                foreach (XmlNode book in property)
                {
                    string xml1 = "<property>" + book.InnerXml.Trim() + "</property>";
                    Property prop = DeserializeFromXML(xml1);
                    if (prop.PropertyID > 0)
                        objProperty.Property.Add(prop);
                }
                ImportXML(objProperty);
            }
            Environment.Exit(0);
        }

        private static void Completed(object sender, AsyncCompletedEventArgs e)
        {
            xmlRead();
        }
        public static Property DeserializeFromXML(string xml)
        {
            Property rootDataObj = new Property();
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Property));
            using (StringReader xmlReader = new StringReader(xml))
            {
                try
                {
                    rootDataObj = (Property)xmlSerializer.Deserialize(xmlReader);
                }
                catch (Exception ex)
                {


                }

            }

            return rootDataObj;
        }
        private static void ImportXML(Properties objProperties)
        {
            DataSet ds = new DataSet();

            //Propeties
            DataTable dt = new DataTable();
            dt.Columns.Add("propertyID", typeof(String));
            dt.Columns.Add("branchID", typeof(String));
            dt.Columns.Add("clientName", typeof(String));
            dt.Columns.Add("branchName", typeof(String));
            dt.Columns.Add("department", typeof(String));
            dt.Columns.Add("referenceNumber", typeof(String));
            dt.Columns.Add("addressName", typeof(String));
            dt.Columns.Add("addressNumber", typeof(String));
            dt.Columns.Add("addressStreet", typeof(String));
            dt.Columns.Add("address2", typeof(String));
            dt.Columns.Add("address3", typeof(String));
            dt.Columns.Add("address4", typeof(String));
            dt.Columns.Add("addressPostcode", typeof(String));
            dt.Columns.Add("country", typeof(String));
            dt.Columns.Add("displayAddress", typeof(String));
            dt.Columns.Add("propertyBedrooms", typeof(String));
            dt.Columns.Add("propertyBathrooms", typeof(String));
            dt.Columns.Add("propertyEnsuites", typeof(String));
            dt.Columns.Add("propertyReceptionRooms", typeof(String));
            dt.Columns.Add("propertyKitchens", typeof(String));
            dt.Columns.Add("displayPropertyType", typeof(String));
            dt.Columns.Add("propertyType", typeof(String));
            dt.Columns.Add("propertyStyle", typeof(String));
            dt.Columns.Add("propertyAge", typeof(String));
            dt.Columns.Add("floorArea", typeof(String));
            dt.Columns.Add("floorAreaUnits", typeof(String));
            dt.Columns.Add("propertyFeature1", typeof(String));
            dt.Columns.Add("propertyFeature2", typeof(String));
            dt.Columns.Add("propertyFeature3", typeof(String));
            dt.Columns.Add("propertyFeature4", typeof(String));
            dt.Columns.Add("propertyFeature5", typeof(String));
            dt.Columns.Add("propertyFeature6", typeof(String));
            dt.Columns.Add("propertyFeature7", typeof(String));
            dt.Columns.Add("propertyFeature8", typeof(String));
            dt.Columns.Add("propertyFeature9", typeof(String));
            dt.Columns.Add("propertyFeature10", typeof(String));
            dt.Columns.Add("price", typeof(String));
            dt.Columns.Add("forSalePOA", typeof(String));
            dt.Columns.Add("priceQualifier", typeof(String));
            dt.Columns.Add("propertyTenure", typeof(String));
            dt.Columns.Add("saleBy", typeof(String));
            dt.Columns.Add("developmentOpportunity", typeof(String));
            dt.Columns.Add("investmentOpportunity", typeof(String));
            dt.Columns.Add("estimatedRentalIncome", typeof(String));
            dt.Columns.Add("availability", typeof(String));
            dt.Columns.Add("mainSummary", typeof(String));
            dt.Columns.Add("fullDescription", typeof(String));
            dt.Columns.Add("dateLastModified", typeof(String));
            dt.Columns.Add("timeLastModified", typeof(String));
            dt.Columns.Add("featuredProperty", typeof(String));
            dt.Columns.Add("regionID", typeof(String));
            dt.Columns.Add("latitude", typeof(String));
            dt.Columns.Add("longitude", typeof(String));

            dt.Columns.Add("rent", typeof(String));
            dt.Columns.Add("rentFrequency", typeof(String));
            dt.Columns.Add("studentProperty", typeof(String));
            dt.Columns.Add("lettingFeePolicyHeadline", typeof(String));
            dt.Columns.Add("lettingFeePolicyDetails", typeof(String));
            dt.Columns.Add("forSale", typeof(String));
            dt.Columns.Add("toLet", typeof(String));
            dt.Columns.Add("priceTo", typeof(String));
            dt.Columns.Add("priceFrom", typeof(String));
            dt.Columns.Add("rentTo", typeof(String));
            dt.Columns.Add("rentFrom", typeof(String));

            dt.Columns.Add("toLetPOA", typeof(String));
            dt.Columns.Add("floorAreaTo", typeof(String));
            dt.Columns.Add("floorAreaFrom", typeof(String));
            dt.Columns.Add("siteArea", typeof(String));
            dt.Columns.Add("siteAreaUnits", typeof(String));
            dt.Columns.Add("strapLine", typeof(String));
            dt.Columns.Add("landAreaTo", typeof(String));
            dt.Columns.Add("landAreaFrom", typeof(String));
            dt.Columns.Add("landAreaUnits", typeof(String));


            //Images
            DataTable dtImages = new DataTable();
            dtImages.Columns.Add("PropertyId", typeof(String));
            dtImages.Columns.Add("Modified", typeof(String));
            dtImages.Columns.Add("ImageUrl", typeof(String));

            //Brouchers
            DataTable dtBrouchers = new DataTable();
            dtBrouchers.Columns.Add("PropertyId", typeof(String));
            dtBrouchers.Columns.Add("Modified", typeof(String));
            dtBrouchers.Columns.Add("BroucherUrl", typeof(String));

            //Floor Plan
            DataTable dtFloorPlans = new DataTable();
            dtFloorPlans.Columns.Add("PropertyId", typeof(String));
            dtFloorPlans.Columns.Add("Modified", typeof(String));
            dtFloorPlans.Columns.Add("FloorplanUrl", typeof(String));

            //Property Types
            DataTable dtPropertyTypes = new DataTable();
            dtPropertyTypes.Columns.Add("PropertyId", typeof(String));
            dtPropertyTypes.Columns.Add("PropertyTypeId", typeof(String));


            foreach (var obj in objProperties.Property)
            {
                DataRow dr;
                dr = dt.NewRow();
                Type temp = typeof(Property);
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (dt.Columns.Contains(pro.Name))
                    {
                        dr[pro.Name] = pro.GetValue(obj);
                    }

                }
                dt.Rows.Add(dr);
                //For property Images
                if (obj.Images != null)
                {
                    foreach (var item in obj.Images.Image)
                    {
                        DataRow dr1;
                        dr1 = dtImages.NewRow();
                        dr1["Modified"] = item.Modified;
                        dr1["ImageUrl"] = item.Text;
                        dr1["PropertyId"] = obj.PropertyID;
                        dtImages.Rows.Add(dr1);
                    }
                }
                //For property Brouchers
                if (obj.Brochures != null)
                {
                    foreach (var item in obj.Brochures.Brochure)
                    {
                        DataRow dr1;
                        dr1 = dtBrouchers.NewRow();
                        dr1["Modified"] = item.Modified;
                        dr1["BroucherUrl"] = item.Text;
                        dr1["PropertyId"] = obj.PropertyID;
                        dtBrouchers.Rows.Add(dr1);
                    }
                }
                //For property FloorPlan
                //foreach (var item in obj.Floorplans.Floorplan)
                if (obj.Floorplans != null)
                {
                    var item1 = obj.Floorplans.Floorplan;
                    if (item1 != null)
                    {
                        DataRow dr1;
                        dr1 = dtFloorPlans.NewRow();
                        dr1["Modified"] = item1.Modified;
                        dr1["FloorplanUrl"] = item1.Text;
                        dr1["PropertyId"] = obj.PropertyID;
                        dtFloorPlans.Rows.Add(dr1);
                    }
                }

                //For property Types
                //if (obj.PropertyTypes != null)
                //{
                //    foreach (var item in obj.PropertyTypes.PropertyType)
                //    {
                //        DataRow dr1;
                //        dr1 = dtBrouchers.NewRow();
                //        dr1["Modified"] = item.Modified;
                //        dr1["BroucherUrl"] = item.Text;
                //        dr1["PropertyId"] = obj.PropertyID;
                //        dtBrouchers.Rows.Add(dr1);
                //    }
                //}
            }
            ds.Tables.Add(dt);
            ds.Tables.Add(dtImages);
            ds.Tables.Add(dtBrouchers);
            ds.Tables.Add(dtFloorPlans);
            dbSqlHelper.DatasetInsertThroughTypeTable("save_xml", ds, "@type_Property1", "TableType", "0", "@type_Images", "TableType", "1", "@type_Brouchers", "TableType", "2", "@type_Floorplans", "TableType", "3");

        }




    }
}
