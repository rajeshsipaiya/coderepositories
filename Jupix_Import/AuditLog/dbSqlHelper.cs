﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for dbSqlHelper
/// </summary>
public static class dbSqlHelper
{
    public static string connection_string = ConfigurationManager.ConnectionStrings["ConStrng"].ToString();
    private static SqlConnection _conn;
    private static SqlConnection con;
    static SqlCommand cmd;
    static SqlDataAdapter adp;
    public static SqlCommand Exceute_Sp_ReturnMultiValues(string SpName, params string[] para)
    {
        int OutPutIndex = -1;
        string OutPutParamaterName = "";
        try
        {
            string[] outpara;
            con = new SqlConnection(connection_string);
            if (con.State.ToString() == "Closed")
                con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SpName;
            SqlParameter P = new SqlParameter("@Rval", SqlDbType.Int);
            cmd.Parameters.Add("@Rval", SqlDbType.Int);
            cmd.Parameters["@Rval"].Direction = ParameterDirection.ReturnValue;

            for (int i = 0; i < para.Length - 1; i++)
            {
                if (para[i + 1].ToString() == "")
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), System.DBNull.Value);
                }
                else if (para[i + 1].ToString() == "Output")
                {
                    cmd.Parameters.Add(para[i].ToString(), SqlDbType.VarChar, 500);
                    cmd.Parameters[para[i].ToString()].Direction = ParameterDirection.Output;
                    OutPutIndex = i;
                    OutPutParamaterName = para[i].ToString();
                }
                else
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), para[i + 1].ToString());
                }
                i = i + 1;
            }
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            string sss = ex.Message;

        }
        finally
        {
            con.Close();
        }

        return cmd;
    }
    public static string Exceute_Sp(string SpName, params string[] para)
    {
        int OutPutIndex = -1;
        string OutPutParamaterName = "";
        try
        {
            string[] outpara;
            con = new SqlConnection(connection_string);
            if (con.State.ToString() == "Closed")
                con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SpName;
            SqlParameter P = new SqlParameter("@Rval", SqlDbType.Int);
            cmd.Parameters.Add("@Rval", SqlDbType.Int);
            cmd.Parameters["@Rval"].Direction = ParameterDirection.ReturnValue;

            for (int i = 0; i < para.Length - 1; i++)
            {
                if (para[i + 1].ToString() == "")
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), System.DBNull.Value);
                }
                else if (para[i + 1].ToString() == "Output")
                {
                    cmd.Parameters.Add(para[i].ToString(), SqlDbType.VarChar, 50);
                    cmd.Parameters[para[i].ToString()].Direction = ParameterDirection.Output;
                    OutPutIndex = i;
                    OutPutParamaterName = para[i].ToString();
                }
                else
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), para[i + 1].ToString());
                }
                i = i + 1;
            }
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {


        }
        finally
        {
            con.Close();
        }

        if (OutPutIndex == -1)
        {
            return Convert.ToString(cmd.Parameters["@Rval"].Value);
        }
        else
        {
            return Convert.ToString(cmd.Parameters[OutPutParamaterName].Value);
        }
    }
    public static string DatasetInsertThroughTypeTable(string SpName, DataSet ds, params string[] para)
    {
        int OutPutIndex = -1;
        string OutPutParamaterName = "";
        try
        {
            string[] outpara;
            con = new SqlConnection(connection_string);
            if (con.State.ToString() == "Closed")
                con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SpName;
            SqlParameter P = new SqlParameter("@Rval", SqlDbType.Int);
            cmd.Parameters.Add("@Rval", SqlDbType.Int);
            cmd.Parameters["@Rval"].Direction = ParameterDirection.ReturnValue;

            for (int i = 0; i < para.Length - 1; i++)
            {
                if (para[i + 1].ToString() == "")
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), System.DBNull.Value);
                }
                else if (para[i + 1].ToString() == "Output")
                {
                    cmd.Parameters.Add(para[i].ToString(), SqlDbType.VarChar, 50);
                    cmd.Parameters[para[i].ToString()].Direction = ParameterDirection.Output;
                    OutPutIndex = i;
                    OutPutParamaterName = para[i].ToString();
                }
                else if (para[i + 1].ToString() == "TableType")
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), ds.Tables[Convert.ToInt32(para[i + 2])]); // passing Datatable  
                    i = i + 1;
                }
                else
                {
                    cmd.Parameters.AddWithValue(para[i].ToString(), para[i + 1].ToString());
                }
                i = i + 1;
            }
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {


        }
        finally
        {
            con.Close();
        }

        if (OutPutIndex == -1)
        {
            return Convert.ToString(cmd.Parameters["@Rval"].Value);
        }
        else
        {
            return Convert.ToString(cmd.Parameters[OutPutParamaterName].Value);
        }


    }
}