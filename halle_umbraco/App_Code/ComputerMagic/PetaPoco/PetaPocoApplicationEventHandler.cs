﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
using UmbracoV7.App_Code.ComputerMagic.PetaPoco.Models;
using Umbraco.Core;


namespace UmbracoV7.App_Code.ComputerMagic.PetaPoco
{
    public class PetaPocoApplicationEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var db = applicationContext.DatabaseContext.Database;

            if (!db.TableExist("CMAuthors"))
            {
                db.CreateTable<Author>(false);
            }

            if (!db.TableExist("CMBooks"))
            {
                db.CreateTable<Book>(false);
            }
        }
    }
}
 