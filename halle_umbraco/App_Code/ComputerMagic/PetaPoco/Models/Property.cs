﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
using System.Reflection;

namespace UmbracoV7.App_Code.ComputerMagic.PetaPoco.Models
{
    [TableName("jupix_Property")]
    public class Property
    {


        //public VirtualTours VirtualTours { get; set; }
        public Int64 PrimaryId { get; set; }
        public object ExternalLinks { get; set; }


        public double PropertyID { get; set; }


        public int BranchID { get; set; }


        public string ClientName { get; set; }


        public string BranchName { get; set; }


        public string Department { get; set; }


        public int ReferenceNumber { get; set; }


        public object AddressName { get; set; }


        public string AddressNumber { get; set; }


        public string AddressStreet { get; set; }


        public object Address2 { get; set; }


        public string Address3 { get; set; }


        public string Address4 { get; set; }


        public string AddressPostcode { get; set; }


        public string Country { get; set; }


        public string DisplayAddress { get; set; }


        public int PropertyBedrooms { get; set; }


        public int PropertyBathrooms { get; set; }


        public int PropertyEnsuites { get; set; }


        public int PropertyReceptionRooms { get; set; }


        public int PropertyKitchens { get; set; }


        public string DisplayPropertyType { get; set; }


        public int PropertyType { get; set; }


        public int PropertyStyle { get; set; }


        public int PropertyAge { get; set; }


        public double FloorArea { get; set; }


        public string FloorAreaUnits { get; set; }


        public string PropertyFeature1 { get; set; }


        public string PropertyFeature2 { get; set; }


        public string PropertyFeature3 { get; set; }


        public string PropertyFeature4 { get; set; }


        public string PropertyFeature5 { get; set; }


        public string PropertyFeature6 { get; set; }


        public string PropertyFeature7 { get; set; }


        public string PropertyFeature8 { get; set; }

        public string PropertyFeature9 { get; set; }


        public string PropertyFeature10 { get; set; }


        public int Price { get; set; }


        public int ForSalePOA { get; set; }


        public int PriceQualifier { get; set; }


        public string PropertyTenure { get; set; }


        public int SaleBy { get; set; }


        public int DevelopmentOpportunity { get; set; }


        public int InvestmentOpportunity { get; set; }


        public double EstimatedRentalIncome { get; set; }


        public int Availability { get; set; }


        public string MainSummary { get; set; }


        public string FullDescription { get; set; }


        public string DateLastModified { get; set; }


        public string TimeLastModified { get; set; }


        public int FeaturedProperty { get; set; }


        public int RegionID { get; set; }


        public string Latitude { get; set; }


        public string Longitude { get; set; }


        //public Flags Flags { get; set; }


        //public Images Images { get; set; }


        //public Floorplans Floorplans { get; set; }


        //public Epc Epc { get; set; }


        //public EpcGraphs EpcGraphs { get; set; }


        public object EpcFrontPages { get; set; }


        //public Brochures Brochures { get; set; }


        public int Rent { get; set; }


        public int RentFrequency { get; set; }


        public int ToLetPOA { get; set; }


        public object StudentProperty { get; set; }


        public int ForSale { get; set; }


        public int ToLet { get; set; }


        public int PriceTo { get; set; }


        public int PriceFrom { get; set; }


        public int RentTo { get; set; }


        public int RentFrom { get; set; }


        public double FloorAreaTo { get; set; }


        public double FloorAreaFrom { get; set; }


        public double SiteArea { get; set; }

        public string SiteAreaUnits { get; set; }


        public double StrapLine { get; set; }


        //public PropertyTypes PropertyTypes { get; set; }


        public string ImageUrl { get; set; }

        public List<Images> PropertyImages { get; set; }
    }
    public class Images
    {
        public string ImageUrl { get; set; }
    }

    public class PropertyType
    {
        public string PropertyTypeName { get; set; }
        public List<Property> Properties { get; set; }
    }
    public class PropertyTypes
    {
        public List<PropertyType> lstPropertyTypes { get; set; }

    }
    public class EnumStringAttribute : Attribute
    {
        public EnumStringAttribute(string stringValue)
        {
            this.stringValue = stringValue;
        }
        private string stringValue;
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    public static class ExtenstionClass
    {
        public static string GetStringValue(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());
            // Get the stringvalue attributes  
            EnumStringAttribute[] attribs = fieldInfo.GetCustomAttributes(
                 typeof(EnumStringAttribute), false) as EnumStringAttribute[];
            // Return the first if there was a match.  
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }
    }
    public enum PropertyAge
    {
        [EnumStringAttribute("Not Specified")]
        NotSpecified = 0,
        [EnumStringAttribute("New Build")]
        NewBuild = 1,
        [EnumStringAttribute("Modern")]
        Modern = 2,
        [EnumStringAttribute("1980s to 1990s")]
        str1980To1990 = 3,
        [EnumStringAttribute("1950, 1960s and 1970s")]
        str1950To1970 = 4,
        [EnumStringAttribute("1940s")]
        str1940s = 5,
        [EnumStringAttribute("1920s to 1930s")]
        str1920To1930 = 6,
        [EnumStringAttribute("Edwardian (1901 - 1910)")]
        str1901To1910 = 7,
        [EnumStringAttribute("Victorian (1837 - 1901)")]
        str1837To1901 = 8,
        [EnumStringAttribute("Georgian (1714 - 1830)")]
        str1714To1830 = 9,
        [EnumStringAttribute("Pre 18th Century")]
        Pre18thCentury = 10,
    }

}