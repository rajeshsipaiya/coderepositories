﻿using UmbracoV7.App_Code.ComputerMagic.PetaPoco.Models;
using System.Collections.Generic;
using Umbraco.Core.Persistence;
using System.Text;

namespace UmbracoV7.App_Code.ComputerMagic.PetaPoco.Repository
{
    public static class PropertyData
    {
        public static IList<Property> GetAll()
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            var query = "select top 20 *,(select top 1 ImageUrl from jupix_Images img where img.PropertyId = prop.PropertyId)as ImageUrl from jupix_Property prop";
            //var query = "select top 20 *,'' as ImageUrl from jupix_Property prop";

            //var data= db.Query<Property>(@"select *,(select top 1 ImageUrl from jupix_Images img where img.PropertyId = prop.PropertyId)as ImageUrl from jupix_Property prop", null);
            List<Property> lst = db.Fetch<Property>(query);
            return lst;
        }
        public static Page<Property> GetAllPaged(int Page, int RecordsPerPage)
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            return db.Page<Property>(Page, RecordsPerPage, "SELECT * FROM jupix_Property");
        }
        public static IList<Property> GetFilterData(string propertyId, string bedrooms, string propertyType, string PostalCode, string minprice, string maxprice)
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            StringBuilder strQuery = new StringBuilder();
            strQuery.Append("select top 20 *,(select top 1 ImageUrl from jupix_Images img where img.PropertyId = prop.PropertyId)as ImageUrl from jupix_Property prop where prop.propertyid like '%" + propertyId + "%'");
            //strQuery.Append("select top 20 *,'' as ImageUrl from jupix_Property prop where");
            if (bedrooms != "0")
            {
                strQuery.Append(" and prop.PropertyBedrooms>=" + bedrooms + "");
            }
            if (propertyType != "" && propertyType != "0")
            {
                strQuery.Append(" and prop.department='" + propertyType + "'");
            }
            if (PostalCode != "")
            {
                strQuery.Append(" and prop.addressPostCode like '%" + PostalCode + "%'");
            }
            if (minprice != "0" && maxprice != "999999999999999")
            {
                strQuery.Append(" and prop.price between " + minprice + " and " + maxprice + "");
            }
            else if (minprice != "0")
            {
                strQuery.Append(" and prop.price>= " + minprice);
            }
            else if (maxprice != "999999999999999")
            {
                strQuery.Append(" and prop.price<= " + maxprice);
            }
            return db.Fetch<Property>(strQuery.ToString());
        }
        public static Property GetPropertyDetail(string PropertyId)
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            List<Property> Records = db.Fetch<Property>("SELECT * FROM jupix_Property WHERE PropertyId = @0", PropertyId);

            if (Records.Count > 0)
            {
                List<Images> PropertyImages = db.Fetch<Images>("SELECT * FROM jupix_Images WHERE PropertyId = @0", PropertyId);
                Records[0].PropertyImages = PropertyImages;
                return Records[0];
            }
            else
                return null;
        }

        public static List<PropertyType> GetPropetiesForHomePage()
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            List<PropertyType> propertyTypes = db.Fetch<PropertyType>("SELECT department as PropertyTypeName FROM jupix_Property group by department");

            foreach (PropertyType item in propertyTypes)
            {
                IList<Property> objProperties = GetPropertiesByDepartment(item.PropertyTypeName, "3");
                item.Properties = new List<Property>();
                item.Properties.AddRange(objProperties);
            }

            return propertyTypes;
        }
        public static IList<Property> GetPropertiesByDepartment(string department, string count)
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            var query = "select top " + count + " *,(select top 1 ImageUrl from jupix_Images img where img.PropertyId = prop.PropertyId)as ImageUrl from jupix_Property prop where prop.department='" + department + "'";
            List<Property> lst = db.Fetch<Property>(query);
            return lst;
        }
        public static List<PropertyType> GetPropetiesTypes()
        {
            UmbracoDatabase db = Umbraco.Core.ApplicationContext.Current.DatabaseContext.Database;
            List<PropertyType> propertyTypes = db.Fetch<PropertyType>("SELECT department as PropertyTypeName FROM jupix_Property group by department");
            return propertyTypes;
        }
    }
}